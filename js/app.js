window.onload = function() {
  /* 数字ボタンにイベントリスナを設定 */
  var btns = document.querySelectorAll('.num, .ope');

  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener('click', dispVal, false);
  }

  /* 操作ボタンにイベントリスナを設定 */
  document.querySelector('.eq').addEventListener('click', dispRes, false);
  document.querySelector('.clr').addEventListener('click', clrDisp, false);
  document.querySelector('.del').addEventListener('click', delVal, false);
  document.querySelector('.hist').addEventListener('click', showHist, false);
}

/* 入力を表示 */
var dispVal = function() {
  var disp = document.getElementById('disp');

  if (disp.textContent == '0') {
    // 表示が0なら、入力で上書き
    disp.textContent = this.value;
  } else {
    // 入力を末尾に追加
    disp.textContent += this.value;
  }
}

/* 計算結果を表示 */
var dispRes = function() {
  var disp = document.getElementById('disp');
  disp.textContent = eval(disp.textContent);
}

/* 表示をクリア */
var clrDisp = function() {
  document.getElementById('disp').textContent = '0';
}

/* 入力を取り消す */
var delVal = function() {
  var disp = document.getElementById('disp');
  disp.textContent = disp.textContent.slice(0, -1);
}

/* 履歴を表示 */
var showHist = function() {
  document.getElementById('hist-sec').className = 'current';
  document.querySelector('[data-position="current"]').className = 'right';
}